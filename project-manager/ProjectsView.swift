//
//  ProjectsView.swift
//  Project Manager
//
//  Created by ITHS on 2016-04-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

import UIKit

class ProjectsView: UITableViewController {
    
    var proj: DefaultLists?

    override func viewDidLoad() {
        super.viewDidLoad()

        proj = DefaultLists()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (proj?.getProjects().count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CellView

        var Plist = proj?.getProjects()
        
        cell.pName.text = Plist![indexPath.row]
        
        let number = proj?.getProjectList(cell.pName.text!).count
        cell.pInfo.text = "Items: " + String (number!)
        
        let barV = getBarValue(Plist![indexPath.row])
        cell.pProgress.progress = barV
        if barV == 1 {
            cell.backgroundColor = UIColor(red: 0.4, green: 1.0, blue: 0.2, alpha: 0.7)
        } else {
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            var list: [String] = proj!.getProjects()
            
            let name = list[indexPath.row]
            
            proj?.removeObject(name + "b")
            list.removeAtIndex(indexPath.row)
            proj?.addProjects(list)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let projList: ProjectListView = segue.destinationViewController as! ProjectListView
        
        if segue.identifier == "Cell" {
            let h = sender as! CellView
            projList.title = h.pName.text
        } else {
            projList.title = "New project"
        }
    }
    func getBarValue(lName : String) -> Float {
        let bList : [Bool] = (self.proj?.getProjectListBool(lName))!
        let total = bList.count
            if total == 0 {
                return 0
            }
        
        var dones = 0
        for x in bList {
            if x == true {
                dones += 1
            }
        }
            if dones == 0 {
                return 0
            }
        
        let value: Float = (Float(dones) / Float(total))
        return Float(value)
    }

}






