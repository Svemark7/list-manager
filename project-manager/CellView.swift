//
//  CellView.swift
//  Project Manager
//
//  Created by ITHS on 2016-04-28.
//  Copyright © 2016 ITHS. All rights reserved.
//

import UIKit

class CellView: UITableViewCell {
    
    var defaults = DefaultLists()

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UITextField!
    
    
    @IBOutlet weak var pProgress: UIProgressView!
    @IBOutlet weak var pName: UILabel!
    @IBOutlet weak var pInfo: UILabel!
    @IBOutlet weak var pItemName: UILabel!
    @IBOutlet weak var pSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
