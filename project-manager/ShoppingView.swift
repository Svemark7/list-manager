//
//  ShoppingView.swift
//  Project Manager
//
//  Created by ITHS on 2016-04-26.
//  Copyright © 2016 ITHS. All rights reserved.
//

import UIKit

class ShoppingView: UITableViewController {
    
    var shopp : DefaultLists?

    @IBAction func newList(sender: AnyObject) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        shopp = DefaultLists()
        
    }
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (shopp?.getShopping().count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CellView

        var Slist = shopp?.getShopping()
        
        cell.name.text = Slist![indexPath.row]
        
        let number = shopp?.getShoppingList(cell.name.text!).count
        cell.info.text = "Items: " + String (number!)
        
        var fd: [Double] = shopp!.getShoppingPrice(Slist![indexPath.row])
        
        cell.price.text = getPrice(fd)

        return cell
    }
    

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }


    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            var list: [String] = shopp!.getShopping()
            let name = list[indexPath.row]
            
            list.removeAtIndex(indexPath.row)
            
            shopp?.removeObject(name + "p")
            shopp?.addShopping(list)
            
            
           // let cell = list[indexPath.row]
           // shopp?.removeObject(cell.name.text!)
            
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let shoppList: ShoppingListView = segue.destinationViewController as! ShoppingListView
        
        if segue.identifier == "Cell" {
            let h = sender as! CellView
            shoppList.title = h.name.text
        } else {
            shoppList.title = "New list"
            self.shopp?.addShoppingList("New list", shoppingList: [])
            self.shopp?.addShoppingListPrice([], name: "New list")
        }
    }
    func getPrice(list: [Double]) -> String {
        
        var sum: Double = 0;
        
        for num in list {
            sum += num
        }
        
        return String(sum)
    }
    

}
