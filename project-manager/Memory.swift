//
//  Memory.swift
//  Project Manager
//
//  Created by ITHS on 2016-04-13.
//  Copyright © 2016 ITHS. All rights reserved.
//

import Foundation

class DefaultLists {
    let defaults = NSUserDefaults.standardUserDefaults()
    
    func getShopping() -> [String] {
        let shopping = defaults.objectForKey("Shopping") as? [String] ?? [String]()
        return shopping
    }
    func addShopping(shopping: [String]) {
        defaults.setObject(shopping, forKey: "Shopping")
    }
    func getShoppingList(list: String) -> [String] {
        let shoppingList = defaults.objectForKey(list) as? [String] ?? [String]()
        return shoppingList
    }
    func addShoppingList(list: String, shoppingList: [String]) {
        defaults.setObject(shoppingList, forKey: list)
    }
    
    func getProjects() -> [String] {
        let projects = defaults.objectForKey("Projects") as? [String] ?? [String]()
        return projects
    }
    func addProjects(projects: [String]) {
        defaults.setObject(projects, forKey: "Projects")
    }
    func getProjectList(list: String) -> [String] {
        let projectList = defaults.objectForKey(list) as? [String] ?? [String]()
        return projectList
    }
    func addProjectList(project: [String], name: String) {
        defaults.setObject(project, forKey: name)
    }
    
    func getProjectListBool(list: String) -> [Bool] {
        let projectListBool = defaults.objectForKey(list + "b") as? [Bool] ?? [Bool]()
        return projectListBool
    }
    func addProjectListBool(states: [Bool], name: String) {
        defaults.setObject(states, forKey: name + "b")
    }
    func getShoppingPrice(list: String) -> [Double] {
        let shoppingPrice = defaults.objectForKey(list + "p") as? [Double] ?? [Double]()
        return shoppingPrice
    }
    func addShoppingListPrice(prices: [Double], name: String) {
        defaults.setObject(prices, forKey: name + "p")
    }
    func removeObject(key: String) {
        defaults.removeObjectForKey(key)
    }
}





