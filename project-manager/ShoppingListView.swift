//
//  ShoppingListView.swift
//  Project Manager
//
//  Created by ITHS on 2016-04-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

import UIKit

class ShoppingListView: UITableViewController, UITextFieldDelegate {
    
    var shopp : DefaultLists?
    var prices : [Double]!
    
    @IBOutlet weak var addNew: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shopp = DefaultLists()
        
        if shopp?.getShoppingList(self.title!).count == 0 {
            addNewItem()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (shopp?.getShoppingList(self.title!).count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CellView
        
        var Slist = shopp?.getShoppingList(self.title!)
        var prices = shopp?.getShoppingPrice(self.title!)
        
        cell.itemName.text = Slist![indexPath.row]
        
        cell.itemPrice.delegate = self
        cell.itemPrice.tag = indexPath.row
        
        
        if prices?.count > 0 {
        switch prices![indexPath.row] {
        case 0:
            print("case 0")
            print(prices![indexPath.row])
            cell.itemPrice.text = ""
        default:
            print("default")
            print(prices![indexPath.row])
            cell.itemPrice.text = "\(prices![indexPath.row])"
        }
        }
        
        cell.selectionStyle = .None

        return cell
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        var prices: [Double] = shopp!.getShoppingPrice(self.title!)
        var tField = textField.text!
        
        print(textField.tag)
        if(tField == "") {
            prices[textField.tag] = 0
        } else {
            prices[textField.tag] = Double(tField)!
        }
        shopp?.addShoppingListPrice(prices, name: self.title!)
    }

    // Override to support conditonal editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
            if editingStyle == .Delete {
                
                var prices = shopp?.getShoppingPrice(self.title!)
                var list: [String] = shopp!.getShoppingList(self.title!)
                
                prices!.removeAtIndex(indexPath.row)
                list.removeAtIndex(indexPath.row)
                shopp?.addShoppingListPrice(prices!, name: self.title!)
                shopp?.addShoppingList(self.title!, shoppingList: list)
                
               // objects.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            } else if editingStyle == .Insert {
                // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
            }
        
    }

 
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func saveList(sender: AnyObject) {
        
        if self.title == "New list" {
        
            let alert = UIAlertController(title: "New list", message: "List name", preferredStyle: .Alert)
            let saveAction = UIAlertAction(title: "Save",
                                           style: .Default, handler: {(action:UIAlertAction) -> Void in
                                        
                                            var list = (self.shopp?.getShopping())!
                                            let itemList = (self.shopp?.getShoppingList(self.title!))!
                                            var prices: [Double] = self.shopp!.getShoppingPrice(self.title!)
                                            
                                            let textField = alert.textFields!.first
                                            
                                            var singleName = true
                                            for name in list {
                                                if name == textField!.text {
                                                    singleName = false
                                                }
                                            }
                                            if singleName {
                                                list.append(textField!.text!)
                                                self.shopp?.addShopping(list)
                                                self.shopp?.addShoppingList(textField!.text!, shoppingList: itemList)
                                                self.shopp?.addShoppingListPrice(prices, name: textField!.text!)
                                                self.navigationController!.popViewControllerAnimated(true)
                                            } else {
                                                let alert = UIAlertController(title: "Dupplicate names", message: "Use different name", preferredStyle: .Alert)
                                                
                                                let okAction = UIAlertAction(title: "Ok", style: .Default)
                                                {(action: UIAlertAction) -> Void in
                                                }
                                                alert.addAction(okAction)
                                                
                                                self.presentViewController(alert, animated: true, completion: nil)
                                            }
            })
        
            let cancelAction = UIAlertAction(title: "Cancel", style: .Default)
            {(action: UIAlertAction) -> Void in
            }
        
            alert.addTextFieldWithConfigurationHandler {
                (UITextField: UITextField) -> Void in
            }
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
        
            presentViewController(alert, animated: true, completion: nil)
        } else {
            var prices: [Double] = shopp!.getShoppingPrice(self.title!)
            
            self.shopp?.addShoppingListPrice(prices, name: self.title!)
    
            self.navigationController!.popViewControllerAnimated(true)
        }
        
    }
    
    @IBAction func addNewClick(sender: AnyObject) {
        
        addNewItem()
        
    }
    
    @IBAction func clearList(sender: AnyObject) {
        
        let alert = UIAlertController(title: "Clear List", message: "Are you sure you want to clear this list?", preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Yes",
                                       style: .Default, handler: {(action:UIAlertAction) -> Void in
                                    
                                        if (self.title == "New list") {
                                            self.shopp?.addShoppingList(self.title!, shoppingList: [])
                                            self.shopp?.addShoppingListPrice([], name: self.title!)
                                            self.tableView.reloadData()
                                        } else {
                                            self.shopp?.removeObject(self.title!)
                                            self.shopp?.removeObject(self.title! + "p")
                                            self.navigationController!.popViewControllerAnimated(true)
                                        }
        })
        
        let cancelAction = UIAlertAction(title: "No", style: .Default)
        {(action: UIAlertAction) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)

    }
    
    func addNewItem() {
        
        let alert = UIAlertController(title: "New item", message: "Add new item", preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .Default, handler: {(action:UIAlertAction) -> Void in
                                        
                                        var list : [String] = (self.shopp?.getShoppingList(self.title!))!
                                        var prices: [Double] = self.shopp!.getShoppingPrice(self.title!)
                                        
                                        let textField = alert.textFields!.first
                                        list.append(textField!.text!)
                                        prices.append(0)
                                        self.shopp?.addShoppingList(self.title!, shoppingList: list)
                                        self.shopp?.addShoppingListPrice(prices, name: self.title!)
                                        self.tableView.reloadData()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default)
        {(action: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (UITextField: UITextField) -> Void in
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
}


