//
//  ProjectListView.swift
//  Project Manager
//
//  Created by ITHS on 2016-04-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

import UIKit

class ProjectListView: UITableViewController {

    var proj: DefaultLists?
    
    @IBOutlet weak var addNew: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        proj = DefaultLists()
        
        if self.title == "New project" {
            addNewItem()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (proj?.getProjectList(self.title!).count)!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CellView
        
        var pList = proj?.getProjectList(self.title!)
        var bList = proj?.getProjectListBool(self.title!)
        
        cell.pSwitch.tag = indexPath.row
        
        switch bList![indexPath.row] {
        case true:
            cell.pSwitch.on = true
            cell.backgroundColor = UIColor(red: 0.4, green: 1.0, blue: 0.2, alpha: 0.7)
        default:
            cell.pSwitch.on = false
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        cell.pItemName.text = pList![indexPath.row]
        cell.selectionStyle = .None
        
        return cell
    }
    
    // Override to support conditonal editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            var list: [String] = proj!.getProjectList(self.title!)
            var bList : [Bool] = proj!.getProjectListBool(self.title!)
            list.removeAtIndex(indexPath.row)
            bList.removeAtIndex(indexPath.row)
            proj?.addProjectList(list, name: self.title!)
            proj?.addProjectListBool(bList, name: self.title!)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            }
        }
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func saveList(sender: AnyObject) {
        
        let alert = UIAlertController(title: "New list", message: "List name", preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .Default, handler: {(action:UIAlertAction) -> Void in
                                        
                                        var list : [String] = (self.proj?.getProjects())!
                                        var itemList : [String] = (self.proj?.getProjectList(self.title!))!
                                        var bList : [Bool] = (self.proj?.getProjectListBool(self.title!))!
                                        
                                        let textField = alert.textFields!.first
                                        var singleName = true
                                        
                                        for name in list {
                                            if name == textField!.text {
                                                singleName = false
                                            }
                                        }
                                        if singleName {
                                            list.append(textField!.text!)
                                            self.proj?.addProjects(list)
                                            self.proj?.addProjectList(itemList, name: textField!.text!)
                                            self.proj?.addProjectListBool(bList, name: textField!.text!)
                                            self.proj?.addProjectList([], name: "New project")
                                            self.proj?.removeObject("New projectb")
                                            self.navigationController!.popViewControllerAnimated(true)
                                        } else {
                                            let alert = UIAlertController(title: "Dupplicate names", message: "Use different name", preferredStyle: .Alert)
                                            
                                            let okAction = UIAlertAction(title: "Ok", style: .Default)
                                            {(action: UIAlertAction) -> Void in
                                            }
                                            alert.addAction(okAction)
                                            
                                            self.presentViewController(alert, animated: true, completion: nil)
                                        }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default)
        {(action: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (UITextField: UITextField) -> Void in
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func addNewClick(sender: AnyObject) {
        
        addNewItem()
        
    }
    
    @IBAction func clearList(sender: AnyObject) {
        
        let alert = UIAlertController(title: "Clear List", message: "Are you sure you want to clear this list?", preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Yes",
                                       style: .Default, handler: {(action:UIAlertAction) -> Void in
                                        
                                        let newList = []
                                        self.proj?.addProjectList(newList as! [String], name: self.title!)
                                        self.proj?.removeObject(self.title!)
                                        self.proj?.addProjectListBool(newList as! [Bool], name: self.title!)
                                        self.proj?.removeObject(self.title! + "b")
                                        if (self.title == "New project") {
                                            self.tableView.reloadData()
                                        } else {
                                            self.navigationController!.popViewControllerAnimated(true)
                                        }
        })
        
        let cancelAction = UIAlertAction(title: "No", style: .Default)
        {(action: UIAlertAction) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func actionS(sender: UISwitch) {
        var bList = self.proj?.getProjectListBool(self.title!)
        if sender.on == true {
            bList![sender.tag] = true
            self.tableView.reloadData()
        } else {
            bList![sender.tag] = false
            self.tableView.reloadData()
        }
        self.proj?.addProjectListBool(bList!, name: self.title!)
    }
    
    func addNewItem() {
        
        let alert = UIAlertController(title: "New item", message: "Add new item", preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .Default, handler: {(action:UIAlertAction) -> Void in
                                        
                                        var list : [String] = (self.proj?.getProjectList(self.title!))!
                                        var bList : [Bool] = (self.proj?.getProjectListBool(self.title!))!
                                        
                                        let textField = alert.textFields!.first
                                        list.append(textField!.text!)
                                        bList.append(false)
                                        self.proj?.addProjectList(list, name: self.title!)
                                        self.proj?.addProjectListBool(bList, name: self.title!)
                                        self.tableView.reloadData()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default)
        {(action: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (UITextField: UITextField) -> Void in
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
}





